<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><div id="user-content-top" align="center" dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/open-mmlab/mmagic/blob/main/docs/en/_static/image/mmagic-logo.png"><img src="/open-mmlab/mmagic/raw/main/docs/en/_static/image/mmagic-logo.png" width="500px" style="max-width: 100%;"></a>
  <div dir="auto">&nbsp;</div>
  <div align="center" dir="auto">
    <b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多模</font></font></b><font style="vertical-align: inherit;"><b><font style="vertical-align: inherit;">态</font></b><font style="vertical-align: inherit;">A</font></font><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">先进</font></font></b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">G</font></font></b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">生成和</font></font><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">智能</font></font></b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">创造</font><font style="vertical-align: inherit;">(MMagic [em'mædʒɪk])
  </font></font><b><font style="vertical-align: inherit;"></font></b><font style="vertical-align: inherit;"></font></div>
  <div dir="auto">&nbsp;</div>
  <div align="center" dir="auto">
    <b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenMMLab 网站</font></font></b>
    <font style="vertical-align: inherit;"><sup><a href="https://openmmlab.com" rel="nofollow"><i><font style="vertical-align: inherit;">热门</font></i></a></sup><b><font style="vertical-align: inherit;">OpenMMLab 平台</font></b><sup><a href="https://platform.openmmlab.com" rel="nofollow"><i><font style="vertical-align: inherit;">试用一下</font></i></a></sup></font><sup>
      <a href="https://openmmlab.com" rel="nofollow">
        <i><font style="vertical-align: inherit;"></font></i>
      </a>
    </sup>
    &nbsp;&nbsp;&nbsp;&nbsp;
    <b><font style="vertical-align: inherit;"></font></b>
    <sup>
      <a href="https://platform.openmmlab.com" rel="nofollow">
        <i><font style="vertical-align: inherit;"></font></i>
      </a>
    </sup>
  </div>
  <div dir="auto">&nbsp;</div>
<p dir="auto"><a href="https://pypi.org/project/mmagic/" rel="nofollow"><img src="https://camo.githubusercontent.com/514b2d8e6ac64746623a01920d6ca2ba38d267d97b5b35e18f44dd79295da30a/68747470733a2f2f62616467652e667572792e696f2f70792f6d6d616769632e737667" alt="皮伊" data-canonical-src="https://badge.fury.io/py/mmagic.svg" style="max-width: 100%;"></a>
<a href="https://mmagic.readthedocs.io/en/latest/" rel="nofollow"><img src="https://camo.githubusercontent.com/d1465870dd0edfb9b32cd6ed333a1262a3c4c9af207be616580d4e8ca3cdc6ad/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f646f63732d6c61746573742d626c7565" alt="文档" data-canonical-src="https://img.shields.io/badge/docs-latest-blue" style="max-width: 100%;"></a>
<a href="https://github.com/open-mmlab/mmagic/actions"><img src="https://github.com/open-mmlab/mmagic/workflows/build/badge.svg" alt="徽章" style="max-width: 100%;"></a>
<a href="https://codecov.io/gh/open-mmlab/mmagic" rel="nofollow"><img src="https://camo.githubusercontent.com/42e94715624a67c8a71505a37cf97ce8f5da3b1a5fa40a8d2260fa42aa69c3c5/68747470733a2f2f636f6465636f762e696f2f67682f6f70656e2d6d6d6c61622f6d6d616769632f6272616e63682f6d61737465722f67726170682f62616467652e737667" alt="代码科夫" data-canonical-src="https://codecov.io/gh/open-mmlab/mmagic/branch/master/graph/badge.svg" style="max-width: 100%;"></a>
<a href="https://github.com/open-mmlab/mmagic/blob/main/LICENSE"><img src="https://camo.githubusercontent.com/d700a18bd278fdf775c891d09ec912e44c498919dee72089afd1d410a9085b7d/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f6c6963656e73652f6f70656e2d6d6d6c61622f6d6d616769632e737667" alt="执照" data-canonical-src="https://img.shields.io/github/license/open-mmlab/mmagic.svg" style="max-width: 100%;"></a>
<a href="https://github.com/open-mmlab/mmagic/issues"><img src="https://camo.githubusercontent.com/0decb8b6d9137e014805ba2c826305dca89c9f22198a5001de17da19df4f63b4/68747470733a2f2f697369746d61696e7461696e65642e636f6d2f62616467652f6f70656e2f6f70656e2d6d6d6c61622f6d6d616769632e737667" alt="开放式问题" data-canonical-src="https://isitmaintained.com/badge/open/open-mmlab/mmagic.svg" style="max-width: 100%;"></a>
<a href="https://github.com/open-mmlab/mmagic/issues"><img src="https://camo.githubusercontent.com/784a4836086ee6590d9edb87248ad67894d4128ec39a6df0a524defaf742ff2f/68747470733a2f2f697369746d61696e7461696e65642e636f6d2f62616467652f7265736f6c7574696f6e2f6f70656e2d6d6d6c61622f6d6d616769632e737667" alt="问题解决" data-canonical-src="https://isitmaintained.com/badge/resolution/open-mmlab/mmagic.svg" style="max-width: 100%;"></a>
<a href="https://openxlab.org.cn/apps/detail/%E6%94%BF%E6%9D%B0/OpenMMLab-Projects" rel="nofollow"><img src="https://camo.githubusercontent.com/11edf84083a11bacb3d10eed5e85000b6fcc020b98a8578afdd4424698c5b56d/68747470733a2f2f63646e2d7374617469632e6f70656e786c61622e6f72672e636e2f6170702d63656e7465722f6f70656e786c61625f64656d6f2e737667" alt="在 OpenXLab 中打开" data-canonical-src="https://cdn-static.openxlab.org.cn/app-center/openxlab_demo.svg" style="max-width: 100%;"></a></p>
<p dir="auto"><a href="https://mmagic.readthedocs.io/en/latest/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">📘文档</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">|
 </font></font><a href="https://mmagic.readthedocs.io/en/latest/get_started/install.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🛠️安装</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">|
 </font></font><a href="https://mmagic.readthedocs.io/en/latest/model_zoo/overview.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">📊模型动物园</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">|
 </font></font><a href="https://mmagic.readthedocs.io/en/latest/changelog.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🆕更新消息</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">|
 </font></font><a href="https://github.com/open-mmlab/mmagic/projects"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🚀正在进行的项目</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">|
 </font></font><a href="https://github.com/open-mmlab/mmagic/issues"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🤔报告问题</font></font></a></p>
<p dir="auto">English | <a href="/open-mmlab/mmagic/blob/main/README_zh-CN.md">简体中文</a></p>
</div>
<div align="center" dir="auto">
  <a href="https://openmmlab.medium.com/" rel="nofollow">
    <img src="https://user-images.githubusercontent.com/25839884/218352562-cdded397-b0f3-4ca1-b8dd-a60df8dca75b.png" width="3%" alt="" style="max-width: 100%;"></a>
  <a target="_blank" rel="noopener noreferrer nofollow" href="https://user-images.githubusercontent.com/25839884/218346358-56cc8e2f-a2b8-487f-9088-32480cceabcf.png"><img src="https://user-images.githubusercontent.com/25839884/218346358-56cc8e2f-a2b8-487f-9088-32480cceabcf.png" width="3%" alt="" style="max-width: 100%;"></a>
  <a href="https://discord.gg/raweFPmdzG" rel="nofollow">
    <img src="https://user-images.githubusercontent.com/25839884/218347213-c080267f-cbb6-443e-8532-8e1ed9a58ea9.png" width="3%" alt="" style="max-width: 100%;"></a>
  <a target="_blank" rel="noopener noreferrer nofollow" href="https://user-images.githubusercontent.com/25839884/218346358-56cc8e2f-a2b8-487f-9088-32480cceabcf.png"><img src="https://user-images.githubusercontent.com/25839884/218346358-56cc8e2f-a2b8-487f-9088-32480cceabcf.png" width="3%" alt="" style="max-width: 100%;"></a>
  <a href="https://twitter.com/OpenMMLab" rel="nofollow">
    <img src="https://user-images.githubusercontent.com/25839884/218346637-d30c8a0f-3eba-4699-8131-512fb06d46db.png" width="3%" alt="" style="max-width: 100%;"></a>
  <a target="_blank" rel="noopener noreferrer nofollow" href="https://user-images.githubusercontent.com/25839884/218346358-56cc8e2f-a2b8-487f-9088-32480cceabcf.png"><img src="https://user-images.githubusercontent.com/25839884/218346358-56cc8e2f-a2b8-487f-9088-32480cceabcf.png" width="3%" alt="" style="max-width: 100%;"></a>
  <a href="https://www.youtube.com/openmmlab" rel="nofollow">
    <img src="https://user-images.githubusercontent.com/25839884/218346691-ceb2116a-465a-40af-8424-9f30d2348ca9.png" width="3%" alt="" style="max-width: 100%;"></a>
</div>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🚀 新消息</font></font><a target="_blank" rel="noopener noreferrer nofollow" href="https://user-images.githubusercontent.com/12782558/212848161-5e783dd6-11e8-4fe0-bbba-39ffb77730be.png"><img width="35" height="20" src="https://user-images.githubusercontent.com/12782558/212848161-5e783dd6-11e8-4fe0-bbba-39ffb77730be.png" style="max-width: 100%;"></a></h2><a id="user-content--whats-new-" class="anchor" aria-label="永久链接：🚀 新消息" href="#-whats-new-"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">新版本</font></font><a href="https://github.com/open-mmlab/mmagic/releases/tag/v1.2.0"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMagic v1.2.0</font></font></strong></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> [2023年12月18日]：</font></font></h3><a id="user-content-new-release-mmagic-v120-18122023" class="anchor" aria-label="永久链接：新版本 MMagic v1.2.0 [18/12/2023]：" href="#new-release-mmagic-v120-18122023"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们的存储库中发布了一种名为 PowerPaint 的先进且强大的修复算法。</font></font><a href="https://github.com/open-mmlab/mmagic/tree/main/projects/powerpaint"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">点击查看</font></font></a></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们很高兴地宣布发布继承自</font></font><a href="https://github.com/open-mmlab/mmediting"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMEditing</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和</font></font><a href="https://github.com/open-mmlab/mmgeneration"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMGeneration</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">的 MMagic v1.0.0 。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">经过 OpenMMLab 2.0 框架的迭代更新并与 MMGeneration 合并，MMEditing 已成为支持基于 GAN 和 CNN 的低级算法的强大工具。如今，MMEditing 拥抱生成式 AI，并转变为更先进、更全面的 AIGC 工具包：</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMagic</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模态</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">高级</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">生成</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">智能</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">创建</font><font style="vertical-align: inherit;">）</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> MMagic将为研究人员和AIGC爱好者提供更加敏捷、灵活的实验支持，为您的AIGC探索之旅助上一臂之力。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们重点介绍以下新功能。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1. 新车型</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们支持 4 个新任务中的 11 个新模型。</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文本到图像/扩散
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">控制网</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">梦想展位</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">稳定扩散</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">迪斯科扩散</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">滑行</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">引导扩散</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3D 感知一代
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">EG3D</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">图像修复
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">北美航空网</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">雷斯托默</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">斯温红外</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">图像着色
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">色彩化研究所</font></font></li>
</ul>
</li>
</ul>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2. 魔法扩散模型</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对于扩散模型，我们提供以下“魔法”：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持基于Stable Diffusion和Disco Diffusion的图像生成。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持Dreambooth、DreamBooth LoRA等Finetune方式。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 ControlNet 支持文本到图像生成的可控性。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持基于xFormers的加速和优化策略，提高训练和推理效率。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持基于MultiFrame Render的视频生成。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持通过DiffuserWrapper调用基础模型和采样策略。</font></font></li>
</ul>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3. 框架升级</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过使用OpenMMLab 2.0框架的MMEngine和MMCV，MMagic在以下新功能上进行了升级：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">重构DataSample以支持批量维度的组合和拆分。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">重构DataPreprocessor，统一训练和推理过程中各种任务的数据格式。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">重构MultiValLoop和MultiTestLoop，支持生成型指标（如FID）和重建型指标（如SSIM）评估，并支持同时评估多个数据集。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持对本地文件或使用tensorboard和wandb进行可视化。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持 Pytorch 2.0 加速的 33 种以上算法。</font></font></li>
</ul>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMagic已经支持了</font></font></strong><font style="vertical-align: inherit;"></font><a href="https://github.com/open-mmlab/mmediting"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMEditing</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和</font></font><a href="https://github.com/open-mmlab/mmgeneration"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMGeneration</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中的所有任务、模型、指标和损失，</font><font style="vertical-align: inherit;">并基于</font></font><a href="https://github.com/open-mmlab/mmengine"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMEngine</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">统一了所有组件的接口。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请参阅</font></font><a href="/open-mmlab/mmagic/blob/main/docs/en/changelog.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">changelog.md</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">了解详细信息和发布历史记录。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请参考</font></font><a href="/open-mmlab/mmagic/blob/main/docs/en/migration/overview.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">迁移文档从</font></font></a><font style="vertical-align: inherit;"></font><a href="https://github.com/open-mmlab/mmagic/tree/0.x"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">旧版本</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMEditing 0.x迁移</font><font style="vertical-align: inherit;">到新版本 MMagic 1.x 。</font></font></p>
<div id="user-content-table" align="center" dir="auto"></div>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">📄 目录</font></font></h2><a id="user-content--table-of-contents" class="anchor" aria-label="永久链接：📄 目录" href="#-table-of-contents"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="#-introduction"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">📖 简介</font></font></a></li>
<li><a href="#-contributing"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🙌 贡献</font></font></a></li>
<li><a href="#%EF%B8%8F-installation"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🛠️安装</font></font></a></li>
<li><a href="#-model-zoo"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">📊 模型动物园</font></font></a></li>
<li><a href="#-acknowledgement"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🤝致谢</font></font></a></li>
<li><a href="#%EF%B8%8F-citation"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🖊️引用</font></font></a></li>
<li><a href="#-license"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🎫 许可证</font></font></a></li>
<li><a href="#%EF%B8%8F-%EF%B8%8Fopenmmlab-family"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🏗️️OpenMMLab 家族</font></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">📖 简介</font></font></h2><a id="user-content--introduction" class="anchor" aria-label="永久链接：📖 简介" href="#-introduction"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMagic ( </font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Multimodal </font></font></strong><font style="vertical-align: inherit;"></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">A</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> dvanced </font><font style="vertical-align: inherit;">, </font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Generative</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> , and </font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Intelligent </font></font></strong><font style="vertical-align: inherit;"></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Creation</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> ) 是一个继承自</font></font><a href="https://github.com/open-mmlab/mmediting"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMEditing</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和</font><font style="vertical-align: inherit;">MMGeneration</font></font><a href="https://github.com/open-mmlab/mmgeneration"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">的</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">先进且全面的 AIGC 工具包。它是一个基于PyTorch的开源图像和视频编辑生成工具箱。它是</font></font><a href="https://openmmlab.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenMMLab</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">项目的一部分</font><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">目前，MMagic 支持多种图像和视频生成/编辑任务。</font></font></p>
<details open="" class="details-reset border rounded-2">
  <summary class="px-3 py-2">
    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-device-camera-video">
    <path d="M16 3.75v8.5a.75.75 0 0 1-1.136.643L11 10.575v.675A1.75 1.75 0 0 1 9.25 13h-7.5A1.75 1.75 0 0 1 0 11.25v-6.5C0 3.784.784 3 1.75 3h7.5c.966 0 1.75.784 1.75 1.75v.675l3.864-2.318A.75.75 0 0 1 16 3.75Zm-6.5 1a.25.25 0 0 0-.25-.25h-7.5a.25.25 0 0 0-.25.25v6.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-6.5ZM11 8.825l3.5 2.1v-5.85l-3.5 2.1Z"></path>
</svg>
    <span aria-label="视频说明 mmagic_introduction.mp4" class="m-1"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">mmagic_介绍.mp4</font></font></span>
    <span class="dropdown-caret"></span>
  </summary>

  <video src="https://private-user-images.githubusercontent.com/49083766/233564593-7d3d48ed-e843-4432-b610-35e3d257765c.mp4?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRodWIuY29tIiwiYXVkIjoicmF3LmdpdGh1YnVzZXJjb250ZW50LmNvbSIsImtleSI6ImtleTUiLCJleHAiOjE3MTIwMTg0NjksIm5iZiI6MTcxMjAxODE2OSwicGF0aCI6Ii80OTA4Mzc2Ni8yMzM1NjQ1OTMtN2QzZDQ4ZWQtZTg0My00NDMyLWI2MTAtMzVlM2QyNTc3NjVjLm1wND9YLUFtei1BbGdvcml0aG09QVdTNC1ITUFDLVNIQTI1NiZYLUFtei1DcmVkZW50aWFsPUFLSUFWQ09EWUxTQTUzUFFLNFpBJTJGMjAyNDA0MDIlMkZ1cy1lYXN0LTElMkZzMyUyRmF3czRfcmVxdWVzdCZYLUFtei1EYXRlPTIwMjQwNDAyVDAwMzYwOVomWC1BbXotRXhwaXJlcz0zMDAmWC1BbXotU2lnbmF0dXJlPWUzMjgyNGNkYTA2MGEwNDk4ZTA5Nzc0MTc2ZTA2YTJjZjI0YjQzMmIwNzU3Y2FmNmJkMDJkOWEwZDliNjI0NjImWC1BbXotU2lnbmVkSGVhZGVycz1ob3N0JmFjdG9yX2lkPTAma2V5X2lkPTAmcmVwb19pZD0wIn0.p6mGxmugtsubDmIBD3vYIeuWB8mUNha0Azu7NDtDLtE" data-canonical-src="https://private-user-images.githubusercontent.com/49083766/233564593-7d3d48ed-e843-4432-b610-35e3d257765c.mp4?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRodWIuY29tIiwiYXVkIjoicmF3LmdpdGh1YnVzZXJjb250ZW50LmNvbSIsImtleSI6ImtleTUiLCJleHAiOjE3MTIwMTg0NjksIm5iZiI6MTcxMjAxODE2OSwicGF0aCI6Ii80OTA4Mzc2Ni8yMzM1NjQ1OTMtN2QzZDQ4ZWQtZTg0My00NDMyLWI2MTAtMzVlM2QyNTc3NjVjLm1wND9YLUFtei1BbGdvcml0aG09QVdTNC1ITUFDLVNIQTI1NiZYLUFtei1DcmVkZW50aWFsPUFLSUFWQ09EWUxTQTUzUFFLNFpBJTJGMjAyNDA0MDIlMkZ1cy1lYXN0LTElMkZzMyUyRmF3czRfcmVxdWVzdCZYLUFtei1EYXRlPTIwMjQwNDAyVDAwMzYwOVomWC1BbXotRXhwaXJlcz0zMDAmWC1BbXotU2lnbmF0dXJlPWUzMjgyNGNkYTA2MGEwNDk4ZTA5Nzc0MTc2ZTA2YTJjZjI0YjQzMmIwNzU3Y2FmNmJkMDJkOWEwZDliNjI0NjImWC1BbXotU2lnbmVkSGVhZGVycz1ob3N0JmFjdG9yX2lkPTAma2V5X2lkPTAmcmVwb19pZD0wIn0.p6mGxmugtsubDmIBD3vYIeuWB8mUNha0Azu7NDtDLtE" controls="controls" muted="muted" class="d-block rounded-bottom-2 border-top width-fit" style="max-height:640px; min-height: 200px">

  </video>
</details>

<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✨ 主要特点</font></font></h3><a id="user-content--major-features" class="anchor" aria-label="永久链接：✨ 主要特点" href="#-major-features"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最先进的模型</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMagic 提供最先进的生成模型来处理、编辑和合成图像和视频。</font></font></p>
</li>
<li>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">强大且流行的应用程序</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMagic 支持流行和当代的图像修复、文本到图像、3D 感知生成、修复、抠图、超分辨率和生成应用程序。具体来说，MMagic 支持稳定扩散的微调和许多令人兴奋的扩散应用，例如带有 SAM 的 ControlNet Animation。 MMagic 还支持 GAN 插值、GAN 投影、GAN 操作和许多其他流行的 GAN 应用程序。是时候开始您的 AIGC 探索之旅了！</font></font></p>
</li>
<li>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">高效的框架</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过使用OpenMMLab 2.0框架的MMEngine和MMCV，MMagic将编辑框架分解为不同的模块，通过组合不同的模块可以轻松构建定制的编辑器框架。我们可以像玩乐高一样定义训练过程，并提供丰富的组件和策略。在MMagic中，您可以通过不同级别的API完成对训练过程的控制。在</font></font><a href="https://github.com/open-mmlab/mmengine/blob/main/mmengine/model/wrappers/seperate_distributed.py"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMSeparateDistributedDataParallel</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">的支持下</font><font style="vertical-align: inherit;">，可以轻松实现动态架构的分布式训练。</font></font></p>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✨ 最佳实践</font></font></h3><a id="user-content--best-practice" class="anchor" aria-label="永久链接：✨ 最佳实践" href="#-best-practice"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们的主分支上的最佳实践适用于</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Python 3.9+</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PyTorch 2.0+</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></li>
</ul>
<p align="right" dir="auto"><a href="#table"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🔝返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🙌 贡献</font></font></h2><a id="user-content--contributing" class="anchor" aria-label="永久链接：🙌 贡献" href="#-contributing"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">越来越多的社区贡献者加入我们，让我们的仓库变得更好。最近的一些项目是由社区贡献的，包括：</font></font></p>
<ul dir="auto">
<li><a href="/open-mmlab/mmagic/blob/main/configs/stable_diffusion_xl/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SDXL</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">由@okotaku 贡献。</font></font></li>
<li><a href="/open-mmlab/mmagic/blob/main/configs/animatediff/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AnimateDiff</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">由@ElliotQi 贡献。</font></font></li>
<li><a href="/open-mmlab/mmagic/blob/main/configs/vico/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ViCo</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">由@FerryHuang 贡献。</font></font></li>
<li><a href="/open-mmlab/mmagic/blob/main/configs/draggan/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DragGan</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">由@qsun1 贡献。</font></font></li>
<li><a href="/open-mmlab/mmagic/blob/main/configs/fastcomposer/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FastComposer</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">由@xiaomile 贡献。</font></font></li>
</ul>
<p dir="auto"><a href="/open-mmlab/mmagic/blob/main/projects/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">打开Projects</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是为了让大家更方便的向MMagic添加项目。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们感谢所有为改进 MMagic 所做的贡献。</font><font style="vertical-align: inherit;">有关贡献指南的更多详细信息，</font><font style="vertical-align: inherit;">请参阅MMCV 中的</font></font><a href="https://github.com/open-mmlab/mmcv/blob/main/CONTRIBUTING.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CONTRIBUTING.md</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和MMEngine 中的</font></font><a href="https://github.com/open-mmlab/mmengine/blob/main/CONTRIBUTING.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CONTRIBUTING.md 。</font></font></a><font style="vertical-align: inherit;"></font></p>
<p align="right" dir="auto"><a href="#table"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🔝返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🛠️安装</font></font></h2><a id="user-content-️-installation" class="anchor" aria-label="永久链接：🛠️安装" href="#️-installation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMagic 依赖于</font></font><a href="https://pytorch.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PyTorch</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><a href="https://github.com/open-mmlab/mmengine"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMEngine</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和</font></font><a href="https://github.com/open-mmlab/mmcv"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMCV</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。以下是快速安装步骤。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤1.按照</font></font></strong><font style="vertical-align: inherit;"></font><a href="https://pytorch.org/get-started/locally/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">官方说明</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
安装PyTorch </font><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 2.</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
使用</font></font><a href="https://github.com/open-mmlab/mim"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MIM</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装 MMCV、MMEngine 和 MMagic 。</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>pip3 install openmim
mim install mmcv<span class="pl-k">&gt;</span>=2.0.0
mim install mmengine
mim install mmagic</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="pip3 install openmim
mim install mmcv>=2.0.0
mim install mmengine
mim install mmagic" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 3.</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
验证 MMagic 是否已成功安装。</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-c1">cd</span> <span class="pl-k">~</span>
python -c <span class="pl-s"><span class="pl-pds">"</span>import mmagic; print(mmagic.__version__)<span class="pl-pds">"</span></span>
<span class="pl-c"><span class="pl-c">#</span> Example output: 1.0.0</span></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="cd ~
python -c &quot;import mmagic; print(mmagic.__version__)&quot;
# Example output: 1.0.0" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">入门</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">成功安装MMagic后，现在你就可以玩MMagic了！ MMagic只需要几行代码就可以从文本生成图像！</font></font></p>
<div class="highlight highlight-source-python notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-k">from</span> <span class="pl-s1">mmagic</span>.<span class="pl-s1">apis</span> <span class="pl-k">import</span> <span class="pl-v">MMagicInferencer</span>
<span class="pl-s1">sd_inferencer</span> <span class="pl-c1">=</span> <span class="pl-v">MMagicInferencer</span>(<span class="pl-s1">model_name</span><span class="pl-c1">=</span><span class="pl-s">'stable_diffusion'</span>)
<span class="pl-s1">text_prompts</span> <span class="pl-c1">=</span> <span class="pl-s">'A panda is having dinner at KFC'</span>
<span class="pl-s1">result_out_dir</span> <span class="pl-c1">=</span> <span class="pl-s">'output/sd_res.png'</span>
<span class="pl-s1">sd_inferencer</span>.<span class="pl-en">infer</span>(<span class="pl-s1">text</span><span class="pl-c1">=</span><span class="pl-s1">text_prompts</span>, <span class="pl-s1">result_out_dir</span><span class="pl-c1">=</span><span class="pl-s1">result_out_dir</span>)</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="from mmagic.apis import MMagicInferencer
sd_inferencer = MMagicInferencer(model_name='stable_diffusion')
text_prompts = 'A panda is having dinner at KFC'
result_out_dir = 'output/sd_res.png'
sd_inferencer.infer(text=text_prompts, result_out_dir=result_out_dir)" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMagic的基本使用方法</font><font style="vertical-align: inherit;">请参见</font></font><a href="/open-mmlab/mmagic/blob/main/docs/en/get_started/quick_run.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">快速运行</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和</font></font><a href="/open-mmlab/mmagic/blob/main/docs/en/user_guides/inference.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">推理。</font></font></a><font style="vertical-align: inherit;"></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从源代码安装 MMagic</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您还可以通过使用以下命令从源代码安装 MMagic 来试验最新开发的版本而不是稳定版本：</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>git clone https://github.com/open-mmlab/mmagic.git
<span class="pl-c1">cd</span> mmagic
pip3 install -e <span class="pl-c1">.</span></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="git clone https://github.com/open-mmlab/mmagic.git
cd mmagic
pip3 install -e ." tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请参阅</font></font><a href="/open-mmlab/mmagic/blob/main/docs/en/get_started/install.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以获取更详细的说明。</font></font></p>
<p align="right" dir="auto"><a href="#table"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🔝返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">📊 模型动物园</font></font></h2><a id="user-content--model-zoo" class="anchor" aria-label="永久链接：📊 模型动物园" href="#-model-zoo"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div align="center" dir="auto">
  <b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持的算法</font></font></b>
</div>
<table align="center">
  <tbody>
    <tr align="center" valign="bottom">
      <td>
        <b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">条件 GAN</font></font></b>
      </td>
      <td>
        <b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">无条件 GAN</font></font></b>
      </td>
      <td>
        <b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">图像修复</font></font></b>
      </td>
      <td>
        <b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">图像超分辨率</font></font></b>
      </td>
    </tr>
    <tr valign="top">
      <td>
        <ul dir="auto">
            <li><a href="/open-mmlab/mmagic/blob/main/configs/sngan_proj/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SNGAN/投影 GAN (ICLR'2018)</font></font></a></li>
            <li><a href="/open-mmlab/mmagic/blob/main/configs/sagan/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">萨根（ICML'2019）</font></font></a></li>
            <li><a href="/open-mmlab/mmagic/blob/main/configs/biggan/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BIGGAN/BIGGAN-DEEP (ICLR'2018)</font></font></a></li>
      </ul>
      </td>
      <td>
        <ul dir="auto">
          <li><a href="/open-mmlab/mmagic/blob/main/configs/dcgan/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DCGAN（ICLR'2016）</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/wgan-gp/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WGAN-GP（NeurIPS'2017）</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/lsgan/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LSGAN（ICCV'2017）</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/ggan/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GGAN (ArXiv'2017)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/pggan/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PGGAN（ICLR'2018）</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/singan/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SinGAN（ICCV'2019）</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/styleganv1/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">StyleGANV1 (CVPR'2019)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/styleganv2/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">StyleGANV2 (CVPR'2019)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/styleganv3/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">StyleGANV3（NeurIPS'2021）</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/draggan/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">德拉甘 (2023)</font></font></a></li>
        </ul>
      </td>
      <td>
        <ul dir="auto">
          <li><a href="/open-mmlab/mmagic/blob/main/configs/swinir/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SwinIR (ICCVW'2021)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/nafnet/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NAFNet (ECCV'2022)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/restormer/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">风暴者 (CVPR'2022)</font></font></a></li>
        </ul>
      </td>
      <td>
        <ul dir="auto">
          <li><a href="/open-mmlab/mmagic/blob/main/configs/srcnn/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SRCNN (TPAMI'2015)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/srgan_resnet/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SRResNet&amp;SRGAN (CVPR'2016)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/edsr/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">EDSR（CVPR'2017）</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/esrgan/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ESRGAN（ECCV'2018）</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/rdn/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RDN（CVPR'2018）</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/dic/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DIC（CVPR'2020）</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/ttsr/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TTSR（CVPR'2020）</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/glean/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">收集（CVPR'2021）</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/liif/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LIIF（CVPR'2021）</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/real_esrgan/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">真实 ESRGAN (ICCVW'2021)</font></font></a></li>
        </ul>
      </td>
    </tr>

    
  </tbody>
<tbody>
    <tr align="center" valign="bottom">
      <td>
        <b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">视频超分辨率</font></font></b>
      </td>
      <td>
        <b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">视频插值</font></font></b>
      </td>
      <td>
        <b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">图像着色</font></font></b>
      </td>
      <td>
        <b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">图像翻译</font></font></b>
      </td>
    </tr>
    <tr valign="top">
      <td>
        <ul dir="auto">
            <li><a href="/open-mmlab/mmagic/blob/main/configs/edvr/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">EDVR（CVPR'2018）</font></font></a></li>
            <li><a href="/open-mmlab/mmagic/blob/main/configs/tof/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TOF（IJCV'2019）</font></font></a></li>
            <li><a href="/open-mmlab/mmagic/blob/main/configs/tdan/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TDAN（CVPR'2020）</font></font></a></li>
            <li><a href="/open-mmlab/mmagic/blob/main/configs/basicvsr/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基本 VSR (CVPR'2021)</font></font></a></li>
            <li><a href="/open-mmlab/mmagic/blob/main/configs/iconvsr/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">IconVSR (CVPR'2021)</font></font></a></li>
            <li><a href="/open-mmlab/mmagic/blob/main/configs/basicvsr_pp/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基本VSR++ (CVPR'2022)</font></font></a></li>
            <li><a href="/open-mmlab/mmagic/blob/main/configs/real_basicvsr/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RealBasicVSR (CVPR'2022)</font></font></a></li>
      </ul>
      </td>
      <td>
        <ul dir="auto">
          <li><a href="/open-mmlab/mmagic/blob/main/configs/tof/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TOFlow (IJCV'2019)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/cain/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">凯恩（AAAI'2020）</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/flavr/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FLAVR（CVPR'2021）</font></font></a></li>
        </ul>
      </td>
      <td>
        <ul dir="auto">
          <li><a href="/open-mmlab/mmagic/blob/main/configs/inst_colorization/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">InstColorization（CVPR'2020）</font></font></a></li>
        </ul>
      </td>
      <td>
        <ul dir="auto">
          <li><a href="/open-mmlab/mmagic/blob/main/configs/pix2pix/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Pix2Pix（CVPR'2017）</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/cyclegan/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CycleGAN (ICCV'2017)</font></font></a></li>
        </ul>
      </td>
    </tr>

    
  </tbody>
<tbody>
    <tr align="center" valign="bottom">
      <td>
        <b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">修复</font></font></b>
      </td>
      <td>
        <b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">消光</font></font></b>
      </td>
      <td>
        <b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文字转图像（视频）</font></font></b>
      </td>
      <td>
        <b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3D 感知一代</font></font></b>
      </td>
    </tr>
    <tr valign="top">
      <td>
        <ul dir="auto">
          <li><a href="/open-mmlab/mmagic/blob/main/configs/global_local/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">全球与本地 (ToG'2017)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/deepfillv1/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DeepFillv1 (CVPR'2018)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/partial_conv/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PConv (ECCV'2018)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/deepfillv2/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DeepFillv2（CVPR'2019）</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/aot_gan/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AOT-GAN (TVCG'2019)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/stable_diffusion/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">稳定扩散修复 (CVPR'2022)</font></font></a></li>
        </ul>
      </td>
      <td>
        <ul dir="auto">
          <li><a href="/open-mmlab/mmagic/blob/main/configs/dim/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">昏暗（CVPR'2017）</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/indexnet/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">IndexNet (ICCV'2019)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/gca/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GCA（AAAI'2020）</font></font></a></li>
        </ul>
      </td>
      <td>
        <ul dir="auto">
          <li><a href="/open-mmlab/mmagic/blob/main/projects/glide/configs/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">滑翔（NeurIPS'2021）</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/guided_diffusion/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">引导扩散（NeurIPS'2021）</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/disco_diffusion/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">迪斯科扩散 (2022)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/stable_diffusion/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">稳定扩散 (2022)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/dreambooth/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">梦想展位 (2022)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/textual_inversion/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文本倒置 (2022)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/projects/prompt_to_prompt/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提示到提示 (2022)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/projects/prompt_to_prompt/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">空文本反转 (2022)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/controlnet/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">控制网络 (2023)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/controlnet_animation/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ControlNet 动画 (2023)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/stable_diffusion_xl/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">稳定扩散 XL (2023)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/animatediff/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">动画差异 (2023)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/vico/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">维科 (2023)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/configs/fastcomposer/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">快速作曲家 (2023)</font></font></a></li>
          <li><a href="/open-mmlab/mmagic/blob/main/projects/powerpaint/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">强力彩绘 (2023)</font></font></a></li>
        </ul>
      </td>
      <td>
        <ul dir="auto">
          <li><a href="/open-mmlab/mmagic/blob/main/configs/eg3d/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">EG3D（CVPR'2022）</font></font></a></li>
        </ul>
      </td>
    </tr>

    
  </tbody>
</table>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请参阅</font></font><a href="https://mmagic.readthedocs.io/en/latest/model_zoo/overview.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">model_zoo</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">了解更多详细信息。</font></font></p>
<p align="right" dir="auto"><a href="#table"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🔝返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🤝致谢</font></font></h2><a id="user-content--acknowledgement" class="anchor" aria-label="永久链接：🤝致谢" href="#-acknowledgement"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMagic 是一个开源项目，由来自各个学院和公司的研究人员和工程师贡献。我们希望工具箱和基准能够通过提供灵活的工具包来重新实现现有方法并开发自己的新方法，从而为不断发展的研究社区服务。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们感谢所有实施其方法或添加新功能的贡献者，以及提供宝贵反馈的用户。谢谢你们！</font></font></p>
<a href="https://github.com/open-mmlab/mmagic/graphs/contributors">
  <img src="https://camo.githubusercontent.com/1ee8256d919cf643fc5455f59b60b0432a9df72a536dc4708ca96d1657c99a0e/68747470733a2f2f636f6e747269622e726f636b732f696d6167653f7265706f3d6f70656e2d6d6d6c61622f6d6d61676963" data-canonical-src="https://contrib.rocks/image?repo=open-mmlab/mmagic" style="max-width: 100%;">
</a>
<p align="right" dir="auto"><a href="#table"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🔝返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🖊️引用</font></font></h2><a id="user-content-️-citation" class="anchor" aria-label="永久链接：🖊️ 引用" href="#️-citation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果MMagic对您的研究有帮助，请在下面引用。</font></font></p>
<div class="highlight highlight-text-bibtex notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-k">@misc</span>{<span class="pl-en">mmagic2023</span>,
    <span class="pl-s">title</span> = <span class="pl-s"><span class="pl-pds">{</span>{MMagic}: {OpenMMLab} Multimodal Advanced, Generative, and Intelligent Creation Toolbox<span class="pl-pds">}</span></span>,
    <span class="pl-s">author</span> = <span class="pl-s"><span class="pl-pds">{</span>{MMagic Contributors}<span class="pl-pds">}</span></span>,
    <span class="pl-s">howpublished</span> = <span class="pl-s"><span class="pl-pds">{</span>\url{https://github.com/open-mmlab/mmagic}<span class="pl-pds">}</span></span>,
    <span class="pl-s">year</span> = <span class="pl-s"><span class="pl-pds">{</span>2023<span class="pl-pds">}</span></span>
}</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="@misc{mmagic2023,
    title = {{MMagic}: {OpenMMLab} Multimodal Advanced, Generative, and Intelligent Creation Toolbox},
    author = {{MMagic Contributors}},
    howpublished = {\url{https://github.com/open-mmlab/mmagic}},
    year = {2023}
}" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<div class="highlight highlight-text-bibtex notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-k">@misc</span>{<span class="pl-en">mmediting2022</span>,
    <span class="pl-s">title</span> = <span class="pl-s"><span class="pl-pds">{</span>{MMEditing}: {OpenMMLab} Image and Video Editing Toolbox<span class="pl-pds">}</span></span>,
    <span class="pl-s">author</span> = <span class="pl-s"><span class="pl-pds">{</span>{MMEditing Contributors}<span class="pl-pds">}</span></span>,
    <span class="pl-s">howpublished</span> = <span class="pl-s"><span class="pl-pds">{</span>\url{https://github.com/open-mmlab/mmediting}<span class="pl-pds">}</span></span>,
    <span class="pl-s">year</span> = <span class="pl-s"><span class="pl-pds">{</span>2022<span class="pl-pds">}</span></span>
}</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="@misc{mmediting2022,
    title = {{MMEditing}: {OpenMMLab} Image and Video Editing Toolbox},
    author = {{MMEditing Contributors}},
    howpublished = {\url{https://github.com/open-mmlab/mmediting}},
    year = {2022}
}" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p align="right" dir="auto"><a href="#table"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🔝返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🎫 许可证</font></font></h2><a id="user-content--license" class="anchor" aria-label="永久链接：🎫 许可证" href="#-license"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该项目是在</font></font><a href="/open-mmlab/mmagic/blob/main/LICENSE"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Apache 2.0 许可证</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下发布的。如果您将我们的代码用于商业用途，</font><font style="vertical-align: inherit;">请参阅</font></font><a href="/open-mmlab/mmagic/blob/main/LICENSE"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">许可证</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">进行仔细检查。</font></font></p>
<p align="right" dir="auto"><a href="#table"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🔝返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🏗️️OpenMMLab 家族</font></font></h2><a id="user-content-️-️openmmlab-family" class="anchor" aria-label="永久链接：🏗️️OpenMMLab 家族" href="#️-️openmmlab-family"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://github.com/open-mmlab/mmengine"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMEngine</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：用于训练深度学习模型的 OpenMMLab 基础库。</font></font></li>
<li><a href="https://github.com/open-mmlab/mmcv"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMCV</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：计算机视觉的 OpenMMLab 基础库。</font></font></li>
<li><a href="https://github.com/open-mmlab/mim"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MIM</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：MIM 安装 OpenMMLab 软件包。</font></font></li>
<li><a href="https://github.com/open-mmlab/mmpretrain"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMPreTrain</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：OpenMMLab 预训练工具箱和基准。</font></font></li>
<li><a href="https://github.com/open-mmlab/mmdetection"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMDetection</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：OpenMMLab 检测工具箱和基准测试。</font></font></li>
<li><a href="https://github.com/open-mmlab/mmdetection3d"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMDetection3D</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：OpenMMLab 的下一代通用 3D 对象检测平台。</font></font></li>
<li><a href="https://github.com/open-mmlab/mmrotate"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMRotate</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：OpenMMLab 旋转对象检测工具箱和基准。</font></font></li>
<li><a href="https://github.com/open-mmlab/mmsegmentation"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMSegmentation</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：OpenMMLab 语义分割工具箱和基准。</font></font></li>
<li><a href="https://github.com/open-mmlab/mmocr"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMOCR</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：OpenMMLab 文本检测、识别和理解工具箱。</font></font></li>
<li><a href="https://github.com/open-mmlab/mmpose"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMPose</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：OpenMMLab 姿态估计工具箱和基准。</font></font></li>
<li><a href="https://github.com/open-mmlab/mmhuman3d"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMHuman3D</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：OpenMMLab 3D 人体参数化模型工具箱和基准。</font></font></li>
<li><a href="https://github.com/open-mmlab/mmselfsup"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMSelfSup</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：OpenMMLab 自监督学习工具箱和基准测试。</font></font></li>
<li><a href="https://github.com/open-mmlab/mmrazor"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMRazor</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：OpenMMLab 模型压缩工具箱和基准测试。</font></font></li>
<li><a href="https://github.com/open-mmlab/mmfewshot"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMFewShot</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：OpenMMLab Fewshot 学习工具箱和基准测试。</font></font></li>
<li><a href="https://github.com/open-mmlab/mmaction2"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMAction2</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：OpenMMLab 的下一代动作理解工具箱和基准。</font></font></li>
<li><a href="https://github.com/open-mmlab/mmtracking"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMTracking</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：OpenMMLab 视频感知工具箱和基准测试。</font></font></li>
<li><a href="https://github.com/open-mmlab/mmflow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMFlow</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：OpenMMLab 光流工具箱和基准测试。</font></font></li>
<li><a href="https://github.com/open-mmlab/mmagic"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMagic</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：OpenMMLab 多模式高级、生成和智能创建工具箱。</font></font></li>
<li><a href="https://github.com/open-mmlab/mmdeploy"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMDeploy</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：OpenMMLab 模型部署框架。</font></font></li>
</ul>
<p align="right" dir="auto"><a href="#table"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🔝返回目录</font></font></a></p>
</article></div>
